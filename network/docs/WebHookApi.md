# WebHookApi

All URIs are relative to *https://api.covid19api.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**webhook**](WebHookApi.md#webhook) | **POST** /webhook | Webhook


<a name="webhook"></a>
# **webhook**
> Webhook webhook(webhookRequest)

Webhook

Adds a webhook to be notified of when new daily data is retrieved. The body of the webhook is the same as the response from /summary.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.yalmanbasf.network.model.*

val apiInstance = WebHookApi()
val webhookRequest : WebhookRequest = {"URL":"https://covid19api.com/webhook"} // WebhookRequest | 
try {
    val result : Webhook = apiInstance.webhook(webhookRequest)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling WebHookApi#webhook")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling WebHookApi#webhook")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webhookRequest** | [**WebhookRequest**](WebhookRequest.md)|  |

### Return type

[**Webhook**](Webhook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

