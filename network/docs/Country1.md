
# Country1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | **kotlin.String** |  | 
**slug** | **kotlin.String** |  | 
**provinces** | **kotlin.Array&lt;kotlin.String&gt;** |  | 



