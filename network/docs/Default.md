
# Default

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** |  | 
**description** | **kotlin.String** |  | 
**path** | **kotlin.String** |  | 
**params** | **kotlin.Array&lt;kotlin.String&gt;** |  | 



