# DefinitionApi

All URIs are relative to *https://api.covid19api.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**countries**](DefinitionApi.md#countries) | **GET** /countries | Countries
[**default**](DefinitionApi.md#default) | **GET** / | Default
[**summary**](DefinitionApi.md#summary) | **GET** /summary | Summary


<a name="countries"></a>
# **countries**
> kotlin.Array&lt;Country&gt; countries()

Countries

Returns all the available countries and provinces, as well as the country slug for per country requests.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.yalmanbasf.network.model.*

val apiInstance = DefinitionApi()
try {
    val result : kotlin.Array<Country> = apiInstance.countries()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DefinitionApi#countries")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DefinitionApi#countries")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.Array&lt;Country&gt;**](Country.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="default"></a>
# **default**
> kotlin.Array&lt;Default&gt; default()

Default

List all the current routes available with detail on each.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.yalmanbasf.network.model.*

val apiInstance = DefinitionApi()
try {
    val result : kotlin.Array<Default> = apiInstance.default()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DefinitionApi#default")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DefinitionApi#default")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.Array&lt;Default&gt;**](Default.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="summary"></a>
# **summary**
> Summary summary()

Summary

A summary of new and total cases per country updated daily.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.yalmanbasf.network.model.*

val apiInstance = DefinitionApi()
try {
    val result : Summary = apiInstance.summary()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling DefinitionApi#summary")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling DefinitionApi#summary")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Summary**](Summary.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

