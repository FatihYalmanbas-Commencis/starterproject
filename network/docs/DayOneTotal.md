
# DayOneTotal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | **kotlin.String** |  | 
**province** | **kotlin.String** |  | 
**lat** | **kotlin.Int** |  | 
**lon** | **kotlin.Int** |  | 
**date** | **kotlin.String** |  | 
**cases** | **kotlin.Int** |  | 
**status** | **kotlin.String** |  | 



