
# DailyCountrySummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | **kotlin.String** |  | 
**slug** | **kotlin.String** |  | 
**newConfirmed** | **kotlin.Int** |  | 
**totalConfirmed** | **kotlin.Int** |  | 
**newDeaths** | **kotlin.Int** |  | 
**totalDeaths** | **kotlin.Int** |  | 
**newRecovered** | **kotlin.Int** |  | 
**totalRecovered** | **kotlin.Int** |  | 



