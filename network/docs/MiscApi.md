# MiscApi

All URIs are relative to *https://api.covid19api.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**allData**](MiscApi.md#allData) | **GET** /all | All Data
[**byCountry**](MiscApi.md#byCountry) | **GET** /country/{country}/status/{status} | By Country
[**byCountryLive**](MiscApi.md#byCountryLive) | **GET** /country/{country}/status/{status}/live | By Country Live
[**byCountryTotal**](MiscApi.md#byCountryTotal) | **GET** /total/country/{country}/status/{status} | By Country Total
[**dayOne**](MiscApi.md#dayOne) | **GET** /dayone/country/{country}/status/{status} | Day One
[**dayOneLive**](MiscApi.md#dayOneLive) | **GET** /dayone/country/{country}/status/{status}/live | Day One Live
[**dayOneTotal**](MiscApi.md#dayOneTotal) | **GET** /total/dayone/country/{country}/status/{status} | Day One Total
[**stats**](MiscApi.md#stats) | **GET** /stats | Stats


<a name="allData"></a>
# **allData**
> kotlin.String allData()

All Data

Returns all daily data. This call results in 10MB of data being returned and should be used infrequently.

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.yalmanbasf.network.model.*

val apiInstance = MiscApi()
try {
    val result : kotlin.String = apiInstance.allData()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MiscApi#allData")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MiscApi#allData")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="byCountry"></a>
# **byCountry**
> kotlin.Array&lt;ByCountry&gt; byCountry(country, status)

By Country

Returns all cases by case type for a country. Country must be the slug from /countries or /summary. Cases must be one of: confirmed, recovered, deaths

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.yalmanbasf.network.model.*

val apiInstance = MiscApi()
val country : kotlin.String = spain // kotlin.String | 
val status : kotlin.String = status_example // kotlin.String | 
try {
    val result : kotlin.Array<ByCountry> = apiInstance.byCountry(country, status)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MiscApi#byCountry")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MiscApi#byCountry")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **kotlin.String**|  |
 **status** | **kotlin.String**|  | [enum: confirmed, recovered, deaths]

### Return type

[**kotlin.Array&lt;ByCountry&gt;**](ByCountry.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="byCountryLive"></a>
# **byCountryLive**
> kotlin.Array&lt;ByCountryLive&gt; byCountryLive(country, status)

By Country Live

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.yalmanbasf.network.model.*

val apiInstance = MiscApi()
val country : kotlin.String = spain // kotlin.String | 
val status : kotlin.String = status_example // kotlin.String | 
try {
    val result : kotlin.Array<ByCountryLive> = apiInstance.byCountryLive(country, status)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MiscApi#byCountryLive")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MiscApi#byCountryLive")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **kotlin.String**|  |
 **status** | **kotlin.String**|  | [enum: confirmed, recovered, deaths]

### Return type

[**kotlin.Array&lt;ByCountryLive&gt;**](ByCountryLive.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="byCountryTotal"></a>
# **byCountryTotal**
> kotlin.Array&lt;ByCountryTotal&gt; byCountryTotal(country, status)

By Country Total

Returns all cases by case type for a country. Country must be the slug from /countries or /summary. Cases must be one of: confirmed, recovered, deaths

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.yalmanbasf.network.model.*

val apiInstance = MiscApi()
val country : kotlin.String = spain // kotlin.String | 
val status : kotlin.String = status_example // kotlin.String | 
try {
    val result : kotlin.Array<ByCountryTotal> = apiInstance.byCountryTotal(country, status)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MiscApi#byCountryTotal")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MiscApi#byCountryTotal")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **kotlin.String**|  |
 **status** | **kotlin.String**|  | [enum: confirmed, recovered, deaths]

### Return type

[**kotlin.Array&lt;ByCountryTotal&gt;**](ByCountryTotal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="dayOne"></a>
# **dayOne**
> kotlin.Array&lt;DayOne&gt; dayOne(country, status)

Day One

Returns all cases by case type for a country from the first recorded case. Country must be the Slug from /countries or /summary. Cases must be one of: confirmed, recovered, deaths

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.yalmanbasf.network.model.*

val apiInstance = MiscApi()
val country : kotlin.String = spain // kotlin.String | 
val status : kotlin.String = status_example // kotlin.String | 
try {
    val result : kotlin.Array<DayOne> = apiInstance.dayOne(country, status)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MiscApi#dayOne")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MiscApi#dayOne")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **kotlin.String**|  |
 **status** | **kotlin.String**|  | [enum: confirmed, recovered, deaths]

### Return type

[**kotlin.Array&lt;DayOne&gt;**](DayOne.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="dayOneLive"></a>
# **dayOneLive**
> kotlin.Array&lt;DayOneLive&gt; dayOneLive(country, status)

Day One Live

Returns all cases by case type for a country from the first recorded case with the latest record being the live count. Country must be the Slug from /countries or /summary. Cases must be one of: confirmed, recovered, deaths

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.yalmanbasf.network.model.*

val apiInstance = MiscApi()
val country : kotlin.String = spain // kotlin.String | 
val status : kotlin.String = status_example // kotlin.String | 
try {
    val result : kotlin.Array<DayOneLive> = apiInstance.dayOneLive(country, status)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MiscApi#dayOneLive")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MiscApi#dayOneLive")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **kotlin.String**|  |
 **status** | **kotlin.String**|  | [enum: confirmed, recovered, deaths]

### Return type

[**kotlin.Array&lt;DayOneLive&gt;**](DayOneLive.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="dayOneTotal"></a>
# **dayOneTotal**
> kotlin.Array&lt;DayOneTotal&gt; dayOneTotal(country, status)

Day One Total

Returns all cases by case type for a country from the first recorded case. Country must be the slug from /countries or /summary. Cases must be one of: confirmed, recovered, deaths

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.yalmanbasf.network.model.*

val apiInstance = MiscApi()
val country : kotlin.String = spain // kotlin.String | 
val status : kotlin.String = status_example // kotlin.String | 
try {
    val result : kotlin.Array<DayOneTotal> = apiInstance.dayOneTotal(country, status)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MiscApi#dayOneTotal")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MiscApi#dayOneTotal")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **kotlin.String**|  |
 **status** | **kotlin.String**|  | [enum: confirmed, recovered, deaths]

### Return type

[**kotlin.Array&lt;DayOneTotal&gt;**](DayOneTotal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="stats"></a>
# **stats**
> stats(origin)

Stats

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.yalmanbasf.network.model.*

val apiInstance = MiscApi()
val origin : kotlin.String = http://localhost // kotlin.String | 
try {
    apiInstance.stats(origin)
} catch (e: ClientException) {
    println("4xx response calling MiscApi#stats")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MiscApi#stats")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **origin** | **kotlin.String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

