# LiveApi

All URIs are relative to *https://api.covid19api.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**liveByCountryAndStatus**](LiveApi.md#liveByCountryAndStatus) | **GET** /live/country/{country}/status/{status} | Live By Country And Status
[**liveByCountryAndStatusAfterDate**](LiveApi.md#liveByCountryAndStatusAfterDate) | **GET** /live/country/{country}/status/{status}/date/{date} | Live By Country And Status After Date


<a name="liveByCountryAndStatus"></a>
# **liveByCountryAndStatus**
> kotlin.Array&lt;LiveByCountryAndStatus&gt; liveByCountryAndStatus(country, status)

Live By Country And Status

Returns all live cases by case type for a country. These records are pulled every 10 minutes and are ungrouped. Country must be the slug from /countries or /summary. Cases must be one of: confirmed, recovered, deaths

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.yalmanbasf.network.model.*

val apiInstance = LiveApi()
val country : kotlin.String = spain // kotlin.String | 
val status : kotlin.String = status_example // kotlin.String | 
try {
    val result : kotlin.Array<LiveByCountryAndStatus> = apiInstance.liveByCountryAndStatus(country, status)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveApi#liveByCountryAndStatus")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveApi#liveByCountryAndStatus")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **kotlin.String**|  |
 **status** | **kotlin.String**|  | [enum: confirmed, recovered, deaths]

### Return type

[**kotlin.Array&lt;LiveByCountryAndStatus&gt;**](LiveByCountryAndStatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="liveByCountryAndStatusAfterDate"></a>
# **liveByCountryAndStatusAfterDate**
> kotlin.Array&lt;LiveByCountryAndStatusAfterDate&gt; liveByCountryAndStatusAfterDate(country, status, date)

Live By Country And Status After Date

Returns all live cases by case type for a country after a given date. These records are pulled every 10 minutes and are ungrouped. Country must be the slug from /countries or /summary. Cases must be one of: confirmed, recovered, deaths

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.yalmanbasf.network.model.*

val apiInstance = LiveApi()
val country : kotlin.String = spain // kotlin.String | 
val status : kotlin.String = status_example // kotlin.String | 
val date : org.threeten.bp.LocalDateTime = 2020-03-21T13:13:30Z // org.threeten.bp.LocalDateTime | 
try {
    val result : kotlin.Array<LiveByCountryAndStatusAfterDate> = apiInstance.liveByCountryAndStatusAfterDate(country, status, date)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveApi#liveByCountryAndStatusAfterDate")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveApi#liveByCountryAndStatusAfterDate")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **kotlin.String**|  |
 **status** | **kotlin.String**|  | [enum: confirmed, recovered, deaths]
 **date** | **org.threeten.bp.LocalDateTime**|  |

### Return type

[**kotlin.Array&lt;LiveByCountryAndStatusAfterDate&gt;**](LiveByCountryAndStatusAfterDate.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

