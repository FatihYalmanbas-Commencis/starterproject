
# LiveByCountryAndStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | **kotlin.String** |  | 
**province** | **kotlin.String** |  | 
**lat** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  | 
**lon** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  | 
**date** | **kotlin.String** |  | 
**cases** | **kotlin.Int** |  | 
**status** | **kotlin.String** |  | 



