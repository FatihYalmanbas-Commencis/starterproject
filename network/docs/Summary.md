
# Summary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**countries** | [**kotlin.Array&lt;DailyCountrySummary&gt;**](DailyCountrySummary.md) |  | 
**date** | **kotlin.String** |  | 



