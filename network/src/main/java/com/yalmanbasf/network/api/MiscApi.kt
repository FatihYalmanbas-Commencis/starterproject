package com.yalmanbasf.network.api

import org.openapitools.client.infrastructure.CollectionFormats.*
import retrofit2.http.*
import retrofit2.Call
import retrofit2.Response
import okhttp3.RequestBody
import okhttp3.ResponseBody
import okhttp3.MultipartBody

import com.yalmanbasf.network.model.ByCountry
import com.yalmanbasf.network.model.ByCountryLive
import com.yalmanbasf.network.model.ByCountryTotal
import com.yalmanbasf.network.model.DayOne
import com.yalmanbasf.network.model.DayOneLive
import com.yalmanbasf.network.model.DayOneTotal

interface MiscApi {
    @GET("/all")
    suspend fun allData(): Response<kotlin.String>

    @GET("/country/{country}/status/{status}")
    suspend fun byCountry(@Path("country") country: kotlin.String, @Path("status") status: kotlin.String): Response<kotlin.Array<ByCountry>>

    @GET("/country/{country}/status/{status}/live")
    suspend fun byCountryLive(@Path("country") country: kotlin.String, @Path("status") status: kotlin.String): Response<kotlin.Array<ByCountryLive>>

    @GET("/total/country/{country}/status/{status}")
    suspend fun byCountryTotal(@Path("country") country: kotlin.String, @Path("status") status: kotlin.String): Response<kotlin.Array<ByCountryTotal>>

    @GET("/dayone/country/{country}/status/{status}")
    suspend fun dayOne(@Path("country") country: kotlin.String, @Path("status") status: kotlin.String): Response<kotlin.Array<DayOne>>

    @GET("/dayone/country/{country}/status/{status}/live")
    suspend fun dayOneLive(@Path("country") country: kotlin.String, @Path("status") status: kotlin.String): Response<kotlin.Array<DayOneLive>>

    @GET("/total/dayone/country/{country}/status/{status}")
    suspend fun dayOneTotal(@Path("country") country: kotlin.String, @Path("status") status: kotlin.String): Response<kotlin.Array<DayOneTotal>>

    @GET("/stats")
    suspend fun stats(@Header("Origin") origin: kotlin.String): Response<Unit>

}
