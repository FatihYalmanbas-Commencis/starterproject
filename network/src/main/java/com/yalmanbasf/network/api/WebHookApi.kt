package com.yalmanbasf.network.api

import org.openapitools.client.infrastructure.CollectionFormats.*
import retrofit2.http.*
import retrofit2.Call
import retrofit2.Response
import okhttp3.RequestBody
import okhttp3.ResponseBody
import okhttp3.MultipartBody

import com.yalmanbasf.network.model.Webhook
import com.yalmanbasf.network.model.WebhookRequest

interface WebHookApi {
    @POST("/webhook")
    suspend fun webhook(@Body webhookRequest: WebhookRequest): Response<Webhook>

}
