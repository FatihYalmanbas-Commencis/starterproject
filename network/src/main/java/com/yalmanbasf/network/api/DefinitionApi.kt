package com.yalmanbasf.network.api

import org.openapitools.client.infrastructure.CollectionFormats.*
import retrofit2.http.*
import retrofit2.Call
import retrofit2.Response
import okhttp3.RequestBody
import okhttp3.ResponseBody
import okhttp3.MultipartBody

import com.yalmanbasf.network.model.Country
import com.yalmanbasf.network.model.Default
import com.yalmanbasf.network.model.Summary

interface DefinitionApi {
    @GET("/countries")
    suspend fun countries(): Response<kotlin.Array<Country>>

    @GET("/")
    suspend fun default(): Response<kotlin.Array<Default>>

    @GET("/summary")
    suspend fun summary(): Response<Summary>

}
