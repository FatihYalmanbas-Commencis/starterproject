# org.openapitools.client - Kotlin client library for Coronavirus COVID19 API

## Requires

* Kotlin 1.3.41
* Gradle 4.9

## Build

First, create the gradle wrapper script:

```
gradle wrapper
```

Then, run:

```
./gradlew check assemble
```

This runs all tests and packages the library.

## Features/Implementation Notes

* Supports JSON inputs/outputs, File inputs, and Form inputs.
* Supports collection formats for query parameters: csv, tsv, ssv, pipes.
* Some Kotlin and Java types are fully qualified to avoid conflicts with types defined in OpenAPI definitions.
* Implementation of ApiClient is intended to reduce method counts, specifically to benefit Android targets.

<a name="documentation-for-api-endpoints"></a>
## Documentation for API Endpoints

All URIs are relative to *https://api.covid19api.com*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DefinitionApi* | [**countries**](docs/DefinitionApi.md#countries) | **GET** /countries | Countries
*DefinitionApi* | [**default**](docs/DefinitionApi.md#default) | **GET** / | Default
*DefinitionApi* | [**summary**](docs/DefinitionApi.md#summary) | **GET** /summary | Summary
*LiveApi* | [**liveByCountryAndStatus**](docs/LiveApi.md#livebycountryandstatus) | **GET** /live/country/{country}/status/{status} | Live By Country And Status
*LiveApi* | [**liveByCountryAndStatusAfterDate**](docs/LiveApi.md#livebycountryandstatusafterdate) | **GET** /live/country/{country}/status/{status}/date/{date} | Live By Country And Status After Date
*MiscApi* | [**allData**](docs/MiscApi.md#alldata) | **GET** /all | All Data
*MiscApi* | [**byCountry**](docs/MiscApi.md#bycountry) | **GET** /country/{country}/status/{status} | By Country
*MiscApi* | [**byCountryLive**](docs/MiscApi.md#bycountrylive) | **GET** /country/{country}/status/{status}/live | By Country Live
*MiscApi* | [**byCountryTotal**](docs/MiscApi.md#bycountrytotal) | **GET** /total/country/{country}/status/{status} | By Country Total
*MiscApi* | [**dayOne**](docs/MiscApi.md#dayone) | **GET** /dayone/country/{country}/status/{status} | Day One
*MiscApi* | [**dayOneLive**](docs/MiscApi.md#dayonelive) | **GET** /dayone/country/{country}/status/{status}/live | Day One Live
*MiscApi* | [**dayOneTotal**](docs/MiscApi.md#dayonetotal) | **GET** /total/dayone/country/{country}/status/{status} | Day One Total
*MiscApi* | [**stats**](docs/MiscApi.md#stats) | **GET** /stats | Stats
*WebHookApi* | [**webhook**](docs/WebHookApi.md#webhook) | **POST** /webhook | Webhook


<a name="documentation-for-models"></a>
## Documentation for Models

 - [com.yalmanbasf.network.model.ByCountry](docs/ByCountry.md)
 - [com.yalmanbasf.network.model.ByCountryLive](docs/ByCountryLive.md)
 - [com.yalmanbasf.network.model.ByCountryTotal](docs/ByCountryTotal.md)
 - [com.yalmanbasf.network.model.Country](docs/Country.md)
 - [com.yalmanbasf.network.model.DailyCountrySummary](docs/DailyCountrySummary.md)
 - [com.yalmanbasf.network.model.DayOne](docs/DayOne.md)
 - [com.yalmanbasf.network.model.DayOneLive](docs/DayOneLive.md)
 - [com.yalmanbasf.network.model.DayOneTotal](docs/DayOneTotal.md)
 - [com.yalmanbasf.network.model.Default](docs/Default.md)
 - [com.yalmanbasf.network.model.LiveByCountryAndStatus](docs/LiveByCountryAndStatus.md)
 - [com.yalmanbasf.network.model.LiveByCountryAndStatusAfterDate](docs/LiveByCountryAndStatusAfterDate.md)
 - [com.yalmanbasf.network.model.Summary](docs/Summary.md)
 - [com.yalmanbasf.network.model.Webhook](docs/Webhook.md)
 - [com.yalmanbasf.network.model.WebhookRequest](docs/WebhookRequest.md)


<a name="documentation-for-authorization"></a>
## Documentation for Authorization

All endpoints do not require authorization.
