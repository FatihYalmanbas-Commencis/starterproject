package com.yalmanbasf.starter

import android.app.Application
import androidx.databinding.DataBindingUtil
import com.yalmanbasf.core.model.ApiHolder
import com.yalmanbasf.starter.core.common.navigation.callback.NavigationControllerActivityLifecycleCallback
import com.yalmanbasf.starter.databinding.DefaultBindingComponent
import com.yalmanbasf.starter.di.ModuleProvider
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.openapitools.client.infrastructure.ApiClient
import java.util.*
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSession

class StarterApp : Application() {

    override fun onCreate() {
        super.onCreate()
        DataBindingUtil.setDefaultComponent(DefaultBindingComponent())

        startKoin {
            androidLogger()
            androidContext(this@StarterApp)
            androidFileProperties()
            val appModule = module {
                single<OkHttpClient> {
                    val builder = OkHttpClient.Builder()
                    if (BuildConfig.DEBUG) {
                        val loggingInterceptor = HttpLoggingInterceptor()
                        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                        builder.addInterceptor(loggingInterceptor)
                        builder.hostnameVerifier { _, _ -> true }
                    }
                    builder.build()
                }
                single<ApiHolder> {
                    val mapApiHolder = MapApiHolder()
                    mapApiHolder.initWith(ApiClient(okHttpClient = get()))
                    return@single mapApiHolder
                }
            }

            val installedModules = ServiceLoader.load(ModuleProvider::class.java).map {
                it.getModule()
            }
            modules(appModule)
            modules(installedModules)
        }

        //TODO: Inject using component such as ActivityLifecycleCallbackRegistrar and inject that component with koin
        registerActivityLifecycleCallbacks(NavigationControllerActivityLifecycleCallback())
    }

}
