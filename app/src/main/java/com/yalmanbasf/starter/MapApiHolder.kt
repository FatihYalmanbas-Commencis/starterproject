package com.yalmanbasf.starter

import com.yalmanbasf.core.model.ApiHolder
import org.openapitools.client.infrastructure.ApiClient
import java.util.concurrent.ConcurrentHashMap


internal class MapApiHolder : ApiHolder {

    private lateinit var retrofitClient: ApiClient

    private val serviceMap: MutableMap<Class<Any>, Any> = ConcurrentHashMap()

    internal fun initWith(
        retrofitClient: ApiClient
    ) {
        this.retrofitClient = retrofitClient
    }

    override fun <S : Any> getOrCreateService(clazz: Class<S>): S {
        return serviceMap.getOrPut(clazz as Class<Any>) {
            return@getOrPut retrofitClient.createService(clazz) as S
        } as S
    }
}
