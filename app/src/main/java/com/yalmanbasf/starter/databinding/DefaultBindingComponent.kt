package com.yalmanbasf.starter.databinding

import androidx.databinding.DataBindingComponent
import com.yalmanbasf.view.ui.binding.DefaultProgressBarBindingAdapterImpl
import com.yalmanbasf.view.ui.binding.DefaultRecyclerViewBindingAdapterImpl
import com.yalmanbasf.view.ui.binding.DefaultTextBindingAdapterImpl
import com.yalmanbasf.view.ui.binding.DefaultViewSwitcherBindingAdapterImpl
import com.yalmanbasf.view.ui.binding.ProgressBarBindingAdapter
import com.yalmanbasf.view.ui.binding.RecyclerViewBindingAdapter
import com.yalmanbasf.view.ui.binding.TextBindingAdapter
import com.yalmanbasf.view.ui.binding.ViewSwitcherBindingAdapter

class DefaultBindingComponent : DataBindingComponent {

    override fun getViewSwitcherBindingAdapter(): ViewSwitcherBindingAdapter = DefaultViewSwitcherBindingAdapterImpl()

    override fun getProgressBarBindingAdapter(): ProgressBarBindingAdapter = DefaultProgressBarBindingAdapterImpl()

    override fun getTextBindingAdapter(): TextBindingAdapter = DefaultTextBindingAdapterImpl()

    override fun getRecyclerViewBindingAdapter(): RecyclerViewBindingAdapter = DefaultRecyclerViewBindingAdapterImpl()

}
