package com.yalmanbasf.starter

import okhttp3.Interceptor
import okhttp3.Response

class BasePathInterceptor(private val basePath: String) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val oldUrl = request.url

        val mutablePathSegments = oldUrl.pathSegments
        val newUrlBuilder = oldUrl.newBuilder()
        (mutablePathSegments.size - 1  downTo 0).forEach {
            newUrlBuilder.removePathSegment(it)
        }

        val pathSegments = mutablePathSegments.toMutableList()
        pathSegments.add(0, basePath)
        newUrlBuilder.addPathSegments(pathSegments.joinToString(separator = "/"))

        val newRequestBuilder = request.newBuilder().url(newUrlBuilder.build())

        return chain.proceed(newRequestBuilder.build())
    }

}
