package com.yalmanbasf.starter.di

import org.koin.core.module.Module

/**
 * Defines a kotlin module for an android dynamic-feature module.
 */
interface ModuleProvider {

    fun getModule(): Module

}
