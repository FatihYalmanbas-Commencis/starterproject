package com.yalmanbasf.starter.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import com.yalmanbasf.starter.core.common.navigation.NavigationController
import com.yalmanbasf.starter.databinding.contentView
import com.yalmanbasf.view.ui.binding.DefaultNavigationDelegate
import org.koin.androidx.scope.ScopeActivity
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.compat.ScopeCompat.getViewModel
import org.koin.androidx.viewmodel.ext.android.getViewModel
import org.koin.androidx.viewmodel.scope.getViewModel

class MainActivity : ScopeActivity(),
    NavigationController by DefaultNavigationDelegate(R.navigation.nav_graph_main) {

    private lateinit var viewModel: MainViewModel

    private val binding by contentView<MainActivity, ViewDataBinding>(R.layout.activity_main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = getViewModel()
        binding.lifecycleOwner = this@MainActivity
        binding.setVariable(BR.viewModel, viewModel)
    }
}
