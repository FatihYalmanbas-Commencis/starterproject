package com.yalmanbasf.starter.main

import com.google.auto.service.AutoService
import com.yalmanbasf.starter.di.ModuleProvider
import com.yalmanbasf.starter.model.MainRepository
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module

@AutoService(ModuleProvider::class)
class MainModuleProvider : ModuleProvider {

    override fun getModule(): Module = module {
        scope(named<MainActivity>()) {
            scope(named<SummaryListFragment>()) {
                scoped { MainRepository(androidContext(), get()) }
                viewModel { SummaryListViewModel(get()) }
            }
            viewModel { MainViewModel() }
        }
    }

}
