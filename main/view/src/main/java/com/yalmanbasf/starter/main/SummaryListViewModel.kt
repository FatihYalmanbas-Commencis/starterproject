package com.yalmanbasf.starter.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import com.yalmanbasf.core.model.ModelLiveData
import com.yalmanbasf.core.model.ModelResponse
import com.yalmanbasf.core.model.toModelLiveData
import com.yalmanbasf.starter.model.DailySummaryItemDTO
import com.yalmanbasf.starter.model.MainRepository
import com.yalmanbasf.view.ui.binding.recyclerview.BindingDescription

class SummaryListViewModel(repository: MainRepository) : ViewModel() {

    private val refreshDataEvent: MutableLiveData<Unit> = MutableLiveData()

    init {
        onClickTryAgain()
    }

    private val dailySummaryResult: ModelLiveData<List<DailySummaryItemDTO>> = refreshDataEvent.switchMap {
        return@switchMap repository.getDailySummary()
    }.toModelLiveData()

    val summaries: LiveData<List<BindingDescription>> = dailySummaryResult.successLiveData.map { response ->
        response.data.orEmpty().map {
            BindingDescription(
                R.layout.layout_pet_item,
                BR.dailySummaryItemViewModel,
                it
            )
        }
    }

    val progress: LiveData<Boolean> = dailySummaryResult.map {
        it is ModelResponse.Loading
    }

    fun onClickTryAgain() {
        refreshDataEvent.value = Unit
    }

}
