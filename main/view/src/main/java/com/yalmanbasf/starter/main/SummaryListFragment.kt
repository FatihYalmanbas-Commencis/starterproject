package com.yalmanbasf.starter.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import org.koin.androidx.scope.ScopeFragment
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.core.parameter.parametersOf

const val ID = "42"

class SummaryListFragment : ScopeFragment() {

    private val viewModel: SummaryListViewModel by viewModel { parametersOf(ID) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return DataBindingUtil.inflate<ViewDataBinding>(
            inflater,
            R.layout.fragment_main,
            container,
            false
        ).also {
            it.lifecycleOwner = this@SummaryListFragment
            it.setVariable(BR.viewModel, viewModel)
        }.root
    }
}
