package com.yalmanbasf.starter.model

import com.yalmanbasf.core.model.resources.StringDescription

data class DailySummaryItemDTO(
    internal val id: String,
    val countryName: StringDescription,
    val newConfirmed: StringDescription,
    val totalConfirmed: StringDescription,
    val newDeaths: StringDescription,
    val totalDeaths: StringDescription,
    val newRecovered: StringDescription,
    val totalRecovered: StringDescription
)
