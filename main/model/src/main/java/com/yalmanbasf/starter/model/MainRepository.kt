package com.yalmanbasf.starter.model

import android.content.Context
import androidx.lifecycle.liveData
import com.yalmanbasf.core.model.ApiHolder
import com.yalmanbasf.core.model.BaseRepository
import com.yalmanbasf.core.model.ModelLiveData
import com.yalmanbasf.core.model.ModelResponse
import com.yalmanbasf.core.model.resources.StringDescription
import com.yalmanbasf.core.model.toModelLiveData
import com.yalmanbasf.network.api.DefinitionApi
import com.yalmanbasf.network.model.DailyCountrySummary

class MainRepository(
    context: Context,
    private val apiHolder: ApiHolder
) : BaseRepository(context) {

    fun getDailySummary(): ModelLiveData<List<DailySummaryItemDTO>> {
        return liveData {
            emit(ModelResponse.Loading())
            val result = apiHolder
                .getOrCreateService(DefinitionApi::class.java)
                .summary()
                .toResult()
                .map { summary ->
                    summary?.countries.orEmpty().map { fromCountrySummary(it) }
                }

            emit(result)
        }.toModelLiveData()
    }

    private fun fromCountrySummary(countrySummary: DailyCountrySummary): DailySummaryItemDTO {
        return DailySummaryItemDTO(
            countrySummary.slug,
            StringDescription.withId(
                R.string.main_model_summary_item_countryName,
                countrySummary.country
            ),
            StringDescription.withId(
                R.string.main_model_summary_item_newConfirmed,
                countrySummary.newConfirmed
            ),
            StringDescription.withId(
                R.string.main_model_summary_item_totalConfirmed,
                countrySummary.totalConfirmed
            ),
            StringDescription.withId(
                R.string.main_model_summary_item_newDeaths,
                countrySummary.newDeaths
            ),
            StringDescription.withId(
                R.string.main_model_summary_item_totalDeaths,
                countrySummary.totalDeaths
            ),
            StringDescription.withId(
                R.string.main_model_summary_item_newRecovered,
                countrySummary.newRecovered
            ),
            StringDescription.withId(
                R.string.main_model_summary_item_totalRecovered,
                countrySummary.totalRecovered
            )
        )
    }


}
