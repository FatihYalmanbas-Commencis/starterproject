package com.yalmanbasf.core.model


sealed class ModelResponse<T> {

    class Loading<T> : ModelResponse<T>() {
        override fun <R> map(transform: (T?) -> R?): ModelResponse<R> = Loading()
    }

    class Success<T>(val data: T?) : ModelResponse<T>() {
        override fun <R> map(transform: (T?) -> R?): ModelResponse<R> = Success(transform(data))
    }

    class Failed<T>(val exception: ModelException) : ModelResponse<T>() {
        override fun <R> map(transform: (T?) -> R?): ModelResponse<R> = Failed(exception)
    }

    abstract fun <R> map(transform: (T?) -> R?): ModelResponse<R>
}
