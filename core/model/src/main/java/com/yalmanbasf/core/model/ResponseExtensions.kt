package com.yalmanbasf.core.model

import retrofit2.Response

interface ResponseHandler<T> {

    fun onResponse(response: Response<T>): ModelResponse<T>

}

class DefaultResponseHandler<T>(
    private val successfulHandler: ResponseHandler<T> = DefaultSuccessfulResponseHandlerImpl(),
    private val unsuccessfulHandler: ResponseHandler<T> = DefaultUnsuccessfulResponseHandlerImpl()
) : ResponseHandler<T> {

    override fun onResponse(response: Response<T>): ModelResponse<T> {
        return if (response.isSuccessful) {
            successfulHandler.onResponse(response)
        } else {
            unsuccessfulHandler.onResponse(response)
        }
    }
}
