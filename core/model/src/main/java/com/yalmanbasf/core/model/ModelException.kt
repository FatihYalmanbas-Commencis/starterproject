package com.yalmanbasf.core.model

import com.yalmanbasf.core.model.resources.StringDescription

/**
 * Defines an exception on model layer.
 */
class ModelException(
    val code: Int? = null,
    val messageDescription: StringDescription
) : Exception(messageDescription.description)
