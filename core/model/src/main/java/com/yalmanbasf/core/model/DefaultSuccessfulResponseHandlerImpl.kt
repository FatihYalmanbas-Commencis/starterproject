package com.yalmanbasf.core.model

import retrofit2.Response

class DefaultSuccessfulResponseHandlerImpl<T> : ResponseHandler<T> {

    override fun onResponse(response: Response<T>): ModelResponse.Success<T> {
        return ModelResponse.Success(response.body())
    }

}
