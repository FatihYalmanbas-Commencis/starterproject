package com.yalmanbasf.core.model

/**
 * Creates and caches a api instance.
 */
interface ApiHolder {

    fun <S: Any> getOrCreateService(clazz: Class<S>): S

}
