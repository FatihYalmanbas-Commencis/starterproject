package com.yalmanbasf.core.model.resources

import android.content.res.Resources
import androidx.annotation.StringRes

class StringDescription private constructor(
    @StringRes val descriptionId: Int? = null,
    val description: String? = null,
    val descritionArgs: List<Any> = emptyList<Any>()
) : ResourceDescription<String> {

    @Suppress("SpreadOperator")
    override fun getWithResources(resources: Resources): String {
        return if (descritionArgs.isEmpty()) {
            if (descriptionId != null) {
                resources.getString(descriptionId)
            } else {
                description!!
            }
        } else {
            if (descriptionId != null) {
                resources.getString(descriptionId, *descritionArgs.toTypedArray())
            } else {
                description!!
            }
        }
    }

    companion object Factory {

        fun withId(
            @StringRes stringRes: Int
        ): StringDescription = StringDescription(descriptionId = stringRes)

        fun withId(
            @StringRes stringRes: Int,
            vararg formatArgs: Any
        ): StringDescription = StringDescription(descriptionId = stringRes, descritionArgs = formatArgs.toList())

        fun withText(
            string: String,
            vararg formatArgs: Any = emptyArray()
        ): StringDescription = StringDescription(description = string, descritionArgs = formatArgs.toList())
    }

}
