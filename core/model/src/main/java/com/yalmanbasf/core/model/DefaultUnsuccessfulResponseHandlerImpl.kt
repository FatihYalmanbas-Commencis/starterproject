package com.yalmanbasf.core.model

import com.yalmanbasf.core.model.resources.StringDescription
import retrofit2.Response

class DefaultUnsuccessfulResponseHandlerImpl<T> : ResponseHandler<T> {

    override fun onResponse(response: Response<T>): ModelResponse.Failed<T> {
        val modelException = ModelException(
            response.code(),
            StringDescription.withId(R.string.core_model_general_error_code, response.code())
        )
        return ModelResponse.Failed(modelException)
    }

}
