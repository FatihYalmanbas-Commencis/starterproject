package com.yalmanbasf.core.model

import android.content.Context
import retrofit2.Response

open class BaseRepository(context: Context) {

    // Can be opened or declared public in future releases
    private fun <T> getDefaultResponseHandler(): DefaultResponseHandler<T> = DefaultResponseHandler()

    fun <T> Response<T>.toResult(
        responseHandler: ResponseHandler<T> = getDefaultResponseHandler()
    ): ModelResponse<T> {
        return responseHandler.onResponse(this)
    }

}
