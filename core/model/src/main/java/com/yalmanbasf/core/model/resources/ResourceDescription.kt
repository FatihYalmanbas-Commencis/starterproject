package com.yalmanbasf.core.model.resources

import android.content.res.Resources

/**
 * Describes a resource such as text, drawable, dimension.
 *
 * @param T type of resource
 */
interface ResourceDescription<T> {

    fun getWithResources(resources: Resources) : T

}
