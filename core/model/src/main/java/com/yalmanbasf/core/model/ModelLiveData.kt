package com.yalmanbasf.core.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class ModelLiveData<T> private constructor() : LiveData<ModelResponse<T>>() {

    private val _successLiveData: MutableLiveData<ModelResponse.Success<T>> = MutableLiveData()
    val successLiveData: LiveData<ModelResponse.Success<T>> = _successLiveData

    private val _failLiveData: MutableLiveData<ModelResponse.Failed<T>> = MutableLiveData()
    val failLiveData: LiveData<ModelResponse.Failed<T>> = _failLiveData

    private val _loadingLiveData: MutableLiveData<ModelResponse.Loading<T>> = MutableLiveData()
    val loadingLiveData: LiveData<ModelResponse.Loading<T>> = _loadingLiveData


    init {
        observeForever { changedValue ->
            updateValues(changedValue)
        }
    }

    private fun updateValues(value: ModelResponse<T>?) {
        if (value != null) {
            when (value) {
                is ModelResponse.Success<T> -> _successLiveData.value = value
                is ModelResponse.Failed<T> -> _failLiveData.value = value
                is ModelResponse.Loading<T> -> _loadingLiveData.value = value
            }
        }
    }

    object Factory {

        fun <T> fromLiveData(liveData: LiveData<ModelResponse<T>>): ModelLiveData<T> {
            val result = ModelLiveData<T>()
            liveData.observeForever { response ->
                result.value = response
            }
            return result
        }

    }
}

fun <T> LiveData<ModelResponse<T>>.toModelLiveData(): ModelLiveData<T> {
    return ModelLiveData.Factory.fromLiveData(this)
}
