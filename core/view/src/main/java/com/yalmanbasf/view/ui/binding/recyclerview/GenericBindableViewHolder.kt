package com.yalmanbasf.view.ui.binding.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.RestrictTo
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.recyclerview.widget.RecyclerView

@RestrictTo(RestrictTo.Scope.LIBRARY_GROUP)
class GenericBindableViewHolder(
    parentView: ViewGroup,
    @LayoutRes layoutRedId: Int,
    bindingComponent: DataBindingComponent? = DataBindingUtil.getDefaultComponent()
) : RecyclerView.ViewHolder(parentView.inflate(layoutRedId)), LifecycleOwner {

    internal lateinit var lifecycleRegistry: LifecycleRegistry

    private val binding: ViewDataBinding = DataBindingUtil.bind(itemView, bindingComponent)!!

    override fun getLifecycle() = lifecycleRegistry

    internal fun onInitialized() {
        lifecycleRegistry.currentState = Lifecycle.State.INITIALIZED
        binding.lifecycleOwner = this
    }

    internal fun onAttachToWindow() {
        lifecycleRegistry.currentState = Lifecycle.State.CREATED
        lifecycleRegistry.currentState = Lifecycle.State.STARTED
        lifecycleRegistry.currentState = Lifecycle.State.RESUMED
    }

    internal fun onDetachFromWindow() {
        lifecycleRegistry.currentState = Lifecycle.State.DESTROYED
    }

    fun bind(viewModel: Any, variableId: Int) {
        binding.setVariable(variableId, viewModel)
    }
}

class GenericViewHolderCreator(private val dataBindingComponent: DataBindingComponent?) {

    internal fun create(
        parentView: ViewGroup,
        @LayoutRes layoutRedId: Int
    ): GenericBindableViewHolder {

        val genericViewHolder =
            GenericBindableViewHolder(
                parentView,
                layoutRedId,
                dataBindingComponent
            )
        val lifecycleRegistry = LifecycleRegistry(genericViewHolder)

        genericViewHolder.lifecycleRegistry = lifecycleRegistry

        return genericViewHolder
    }
}


private fun ViewGroup.inflate(@LayoutRes layoutRedId: Int) =
    LayoutInflater.from(context).inflate(layoutRedId, this, false)
