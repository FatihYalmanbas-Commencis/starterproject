/*
 * Copyright 2019 Commencis. All Rights Reserved.
 *
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Commencis.
 * Any reproduction of this material must contain this notice.
 */

package com.yalmanbasf.view.ui.binding.recyclerview

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

class BindableRecyclerViewAdapter(
    private val viewHolderCreator: GenericViewHolderCreator,
    diffItemCallback: DiffUtil.ItemCallback<BindingDescription>
) : ListAdapter<BindingDescription, GenericBindableViewHolder>(diffItemCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, layoutId: Int): GenericBindableViewHolder {
        val viewHolder = viewHolderCreator.create(parent, layoutId)
        viewHolder.onInitialized()
        return viewHolder
    }

    override fun onBindViewHolder(holder: GenericBindableViewHolder, position: Int) {
        val bindingDescription = getItem(position)
        holder.bind(bindingDescription.variable, bindingDescription.variableId)
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).layoutId
    }

    override fun onViewAttachedToWindow(holder: GenericBindableViewHolder) {
        super.onViewAttachedToWindow(holder)
        holder.onAttachToWindow()
    }

    override fun onViewDetachedFromWindow(holder: GenericBindableViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.onDetachFromWindow()
    }
}
