package com.yalmanbasf.view.ui.binding.recyclerview

import androidx.annotation.LayoutRes

data class BindingDescription(
    @LayoutRes val layoutId: Int, val variableId: Int,
    val variable: Any
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BindingDescription

        if (layoutId != other.layoutId) return false
        if (variableId != other.variableId) return false
        if (variable != other.variable) return false

        return true
    }

    override fun hashCode(): Int {
        var result = layoutId
        result = 31 * result + variableId
        result = 31 * result + variable.hashCode()
        return result
    }
}

