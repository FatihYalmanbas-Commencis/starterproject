package com.yalmanbasf.view.ui.binding

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.yalmanbasf.core.model.resources.ResourceDescription

interface TextBindingAdapter {

    @BindingAdapter("textDescription")
    fun setDescribedText(textView: TextView, stringDescription: ResourceDescription<String>)

}

class DefaultTextBindingAdapterImpl : TextBindingAdapter {

    override fun setDescribedText(textView: TextView, stringDescription: ResourceDescription<String>) {
        textView.text = stringDescription.getWithResources(textView.resources)
    }

}
