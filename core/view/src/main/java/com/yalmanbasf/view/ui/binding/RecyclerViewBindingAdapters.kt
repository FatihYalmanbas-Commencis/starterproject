package com.yalmanbasf.view.ui.binding

import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.yalmanbasf.view.ui.binding.recyclerview.BindableRecyclerViewAdapter
import com.yalmanbasf.view.ui.binding.recyclerview.BindingDescription
import com.yalmanbasf.view.ui.binding.recyclerview.DefaultDiffItemCallback
import com.yalmanbasf.view.ui.binding.recyclerview.GenericViewHolderCreator

interface RecyclerViewBindingAdapter {

    @BindingAdapter("bindItems")
    fun bindItems(recyclerView: RecyclerView, items: List<BindingDescription>?)

}

class DefaultRecyclerViewBindingAdapterImpl : RecyclerViewBindingAdapter {

    override fun bindItems(recyclerView: RecyclerView, items: List<BindingDescription>?) {
        if (items == null) {
            return
        }

        if (recyclerView.adapter == null) {
            recyclerView.adapter =
                BindableRecyclerViewAdapter(
                    diffItemCallback = DefaultDiffItemCallback(),
                    viewHolderCreator = GenericViewHolderCreator(
                        DataBindingUtil.getDefaultComponent()
                    )
                )
        }

        (recyclerView.adapter as BindableRecyclerViewAdapter).submitList(items)
    }

}
