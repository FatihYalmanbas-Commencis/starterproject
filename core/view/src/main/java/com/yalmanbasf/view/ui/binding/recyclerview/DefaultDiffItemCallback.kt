package com.yalmanbasf.view.ui.binding.recyclerview

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil

class DefaultDiffItemCallback : DiffUtil.ItemCallback<BindingDescription>() {

    override fun areItemsTheSame(
        oldItem: BindingDescription,
        newItem: BindingDescription
    ): Boolean {
        return oldItem.layoutId == newItem.layoutId && oldItem.variableId == newItem.variableId
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(
        oldItem: BindingDescription,
        newItem: BindingDescription
    ): Boolean {
        return oldItem.variable == newItem.variable
    }

}
