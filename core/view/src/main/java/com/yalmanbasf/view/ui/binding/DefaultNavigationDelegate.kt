package com.yalmanbasf.view.ui.binding

import androidx.annotation.NavigationRes
import com.yalmanbasf.starter.core.common.navigation.NavigationController
import com.yalmanbasf.starter.core.common.navigation.NavigationDelegate
import com.yalmanbasf.view.R

class DefaultNavigationDelegate(
    @NavigationRes navigationGraphId: Int
) : NavigationController by NavigationDelegate(navigationGraphId, R.id.coreViewFragmentContainerView)
