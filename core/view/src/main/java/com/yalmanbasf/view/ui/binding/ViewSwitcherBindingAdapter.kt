package com.yalmanbasf.view.ui.binding

import android.widget.ViewAnimator
import androidx.databinding.BindingAdapter

interface ViewSwitcherBindingAdapter {

    @BindingAdapter("selectedChildIndex")
    fun setChildAt(viewAnimator: ViewAnimator, index: Int)

}

class DefaultViewSwitcherBindingAdapterImpl : ViewSwitcherBindingAdapter {

    override fun setChildAt(viewAnimator: ViewAnimator, index: Int) {
        viewAnimator.displayedChild = index
    }

}
