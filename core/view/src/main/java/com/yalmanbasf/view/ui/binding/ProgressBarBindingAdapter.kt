package com.yalmanbasf.view.ui.binding

import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.core.widget.ContentLoadingProgressBar
import androidx.databinding.BindingAdapter

interface ProgressBarBindingAdapter {

    @BindingAdapter("progressVisibility")
    fun setProgressVisibility(progressBar: ProgressBar, visible: Boolean)

}

class DefaultProgressBarBindingAdapterImpl : ProgressBarBindingAdapter {

    override fun setProgressVisibility(progressBar: ProgressBar, visible: Boolean) {
        if (progressBar is ContentLoadingProgressBar) {
            if (visible) progressBar.show() else progressBar.hide()
        } else {
            progressBar.isVisible = visible
        }
    }

}
