package com.yalmanbasf.starter.core.common.util

import android.app.Activity
import android.app.Application
import android.os.Bundle

open class SimpleActivityLifecycleCallback : Application.ActivityLifecycleCallbacks {
    override fun onActivityPaused(activity: Activity) {
        // Implemented in subclasses
    }

    override fun onActivityStarted(activity: Activity) {
        // Implemented in subclasses
    }

    override fun onActivityDestroyed(activity: Activity) {
        // Implemented in subclasses
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        // Implemented in subclasses
    }

    override fun onActivityStopped(activity: Activity) {
        // Implemented in subclasses
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        // Implemented in subclasses
    }

    override fun onActivityResumed(activity: Activity) {
        // Implemented in subclasses
    }
}
