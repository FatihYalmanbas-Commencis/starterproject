package com.yalmanbasf.starter.core.common.util

import java.lang.ref.WeakReference
import kotlin.reflect.KProperty

inline fun <reified T> weakReference() =
    WeakReferenceDelegate<T>()

inline fun <reified T> weakReference(value: T) =
    WeakReferenceDelegate(
        value
    )

/**
 * Delegates property to a weak reference.
 *
 */
class WeakReferenceDelegate<T> {
    private var weakReference: WeakReference<T>? = null

    constructor()
    constructor(value: T) {
        weakReference = WeakReference(value)
    }

    operator fun getValue(thisRef: Any, property: KProperty<*>): T? = weakReference?.get()
    operator fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        weakReference = WeakReference(value)
    }
}
