package com.yalmanbasf.starter.core.common.navigation

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.NavDirections

/**
 * Handles navigation with lifecycle owner and a navigation holder.
 */
interface NavigationHandler : Observer<NavDirections?> {

    var navController: NavController?

}

/**
 * Holds navigations to be consumed from navigation handler.
 *
 * [NavigationHandler] receives navigation directions and passes it to view controller,
 * updates directions if needed. @sample: Shared element transitions
 */
interface NavigationDirectionHolder {

    fun observeNavigationDirections(lifecycleOwner: LifecycleOwner, navigationHandler: NavigationHandler)

}
