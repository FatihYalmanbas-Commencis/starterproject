package com.yalmanbasf.starter.core.common.navigation

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.annotation.NavigationRes

open class NavigationDelegate(
    @NavigationRes private val navGraph: Int,
    @IdRes private val navigationHostRes: Int,
    private val startDestinationArguments: Bundle? = null
) : NavigationController {

    override fun getNavigationGraph(): Int = navGraph

    override fun getNavigationHostLayoutRes(): Int = navigationHostRes

    override val startDestinationArgs
        get() = startDestinationArguments

}
