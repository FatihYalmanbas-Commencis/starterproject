package com.yalmanbasf.starter.core.common.navigation.callback

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavHost
import androidx.navigation.fragment.NavHostFragment
import com.yalmanbasf.starter.core.common.navigation.NavigationController
import com.yalmanbasf.starter.core.common.navigation.NavigationOwner
import com.yalmanbasf.starter.core.common.util.SimpleActivityLifecycleCallback

/**
 * An activity lifecycle callback that handles creating navigation host fragment at specific content layout.
 */
class NavigationControllerActivityLifecycleCallback : SimpleActivityLifecycleCallback() {

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        if (activity is NavigationController && activity is FragmentActivity) {
            if (savedInstanceState == null) {
                val fragmentManager = activity.supportFragmentManager
                val navHostFragment =
                    NavHostFragment.create(
                        activity.getNavigationGraph(),
                        activity.startDestinationArgs
                    )
                fragmentManager.beginTransaction()
                    .replace(activity.getNavigationHostLayoutRes(), navHostFragment)
                    .setPrimaryNavigationFragment(navHostFragment)
                    .commit()
                fragmentManager.registerFragmentLifecycleCallbacks(
                    NavigationFragmentLifecycleCallback(), true
                )
            }
        }
    }

    override fun onActivityStarted(activity: Activity) {
        if (activity is FragmentActivity && activity is NavigationOwner) {
            activity.navigationHandler.navController =
                (activity.supportFragmentManager.primaryNavigationFragment as NavHost).navController
            activity.navigationDirectionHolder.observeNavigationDirections(
                activity,
                activity.navigationHandler
            )
        }
    }

}
