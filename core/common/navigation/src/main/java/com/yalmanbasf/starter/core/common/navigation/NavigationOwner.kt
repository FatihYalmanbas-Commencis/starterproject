package com.yalmanbasf.starter.core.common.navigation

/**
 * View controller that hosts navigation callbacks
 */
interface NavigationOwner {

    val navigationHandler: NavigationHandler

    val navigationDirectionHolder: NavigationDirectionHolder

}
