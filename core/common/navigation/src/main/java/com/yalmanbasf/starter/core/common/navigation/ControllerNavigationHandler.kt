package com.yalmanbasf.starter.core.common.navigation

import android.util.Log
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import com.yalmanbasf.starter.core.common.util.weakReference

class ControllerNavigationHandler : NavigationHandler {

    // TODO: Find a way to inject by constructor if possible
    override var navController: NavController? by weakReference()

    override fun onChanged(navDirection: NavDirections?) {
        val tempNavController = navController
        if (tempNavController != null) {
            if (navDirection != null) {
                tempNavController.navigate(navDirection)
            }
        } else {
            Log.d("NavigationHandler", "Controller is Null")
        }
    }

}
