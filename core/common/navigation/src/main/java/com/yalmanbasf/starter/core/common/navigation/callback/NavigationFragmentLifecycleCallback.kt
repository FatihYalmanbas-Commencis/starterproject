package com.yalmanbasf.starter.core.common.navigation.callback

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.findNavController
import com.yalmanbasf.starter.core.common.navigation.NavigationOwner

class NavigationFragmentLifecycleCallback : FragmentManager.FragmentLifecycleCallbacks() {

    override fun onFragmentStarted(fm: FragmentManager, fragment: Fragment) {
        super.onFragmentStarted(fm, fragment)
        if (fragment is NavigationOwner) {
            fragment.navigationHandler.navController = fragment.findNavController()
            fragment.navigationDirectionHolder.observeNavigationDirections(
                fragment,
                fragment.navigationHandler
            )
        }
    }
}
