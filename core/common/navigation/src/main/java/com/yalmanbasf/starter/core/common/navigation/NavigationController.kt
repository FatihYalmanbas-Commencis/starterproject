package com.yalmanbasf.starter.core.common.navigation

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.annotation.NavigationRes

/**
 * View controller that hosts android navigation components.
 */
interface NavigationController {

    @NavigationRes
    fun getNavigationGraph(): Int

    @IdRes
    fun getNavigationHostLayoutRes(): Int

    val startDestinationArgs: Bundle?
        get() = null


}
